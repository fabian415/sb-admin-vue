import Overview from '@/views/main/Overview'
import NotFound from '@/views/main/NotFound'
import Blank from '@/views/main/Blank'
import Chart from '@/views/main/Chart'
import Table from '@/views/main/Table'

export default [
    {
        path: '', redirect: 'overview'
    },
    {
        path: 'overview', name: 'overview', component: Overview
    },
    {
        path: 'notfound', name: 'notfound', component: NotFound
    },
    {
        path: 'blank', name: 'blank', component: Blank
    },
    {
        path: 'chart', name: 'chart', component: Chart
    },
    {
        path: 'table', name: 'table', component: Table
    }
]