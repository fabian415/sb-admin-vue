import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/login/Login'
import Register from '@/views/login/Register'
import ForgetPassword from '@/views/login/ForgetPassword'
import Main from '@/views/main/Main'
import menu from './menu'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/login',
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/forgetpassword',
      name: 'forgetpassword',
      component: ForgetPassword
    },
    {
      path: '/main',
      // name: 'main',
      component: Main,
      children: menu,
    },
  ]
})
